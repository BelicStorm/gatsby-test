import React from 'react';
import { Link } from 'gatsby';

import Layout from '../components/Layout';
import Cards from "../components/cards"
import {ProgressBar} from "../components/cv-aptitude-bar"

import sara from '../assets/images/sara.png';


import config from '../../config';
const IndexPage = () => (
  <Layout fullMenu>
    
    <section id="banner">
      <div className="inner">
        <Link to="/"><h2>{config.heading}</h2></Link>
        <p>{config.subHeading}</p>
      </div>
    </section>
    <section id="wrapper">
     <section id="four" className="wrapper alt style1">
        <div className="inner">
          <h2 className="major">Vitae phasellus</h2>
          <p>
            Cras mattis ante fermentum, malesuada neque vitae, eleifend erat.
            Phasellus non pulvinar erat. Fusce tincidunt, nisl eget mattis
            egestas, purus ipsum consequat orci, sit amet lobortis lorem lacus
            in tellus. Sed ac elementum arcu. Quisque placerat auctor laoreet.
          </p>
          <section className="features">
              <Cards page="Front"></Cards>
          </section>
          <ul className="actions">
            <li>
            <Link to="/Portfolio" className="button">Browse All</Link>
            </li>
          </ul>
        </div>
      </section>
     <section id="one" className="wrapper spotlight style1">
        <div className="inner">
          <div className="image">
            <img src={sara} alt="" />
          </div>
          <div className="content">
            <h2 className="major">About Me</h2>
            <p>
              Name - Age - Profesion
            </p>
              <ProgressBar page="Front"></ProgressBar>
            <Link to="/curriculum" className="special">
              Learn more
            </Link>
          </div>
        </div>
      </section>
    </section>
  </Layout>
);

export default IndexPage;
