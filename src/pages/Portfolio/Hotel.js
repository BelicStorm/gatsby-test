import React from 'react';

import Layout from '../../components/Layout';
import Carousel from '../../components/carousel';


const Hotel = () => <Layout fullMenu>
<section id="wrapper">
    <section id="banner">
        <div className="inner">
          <h2>Test1</h2>
          <p>Creación de una habitación de hotel para la segunda edición del concurso InterCIDEC</p>
        </div>
    </section>
      <section id="four" className="wrapper alt style1">
        <div className="inner">
           {/* Contenido a borrar en nueva pagina */}
          <h2>Información del proyecto</h2>
              <p>
                Este proyecto consiste en la creación de una habitación de hotel para la segunda edición 
                del concurso InterCIDEC, celebrado en 2017, organizado por Beltá & Frajumar. Para ello se proporcionó la 
                planta vacía de una habitación de hotel.
              </p>
              <img className="image right med" src="../../Hotel_2017/PlantaOriginal.PNG" alt="PlantaOriginal"></img>
      
              <img className="image left small zoom" src="../../Hotel_2017/mirrorcube-2.jpg" alt="Referencia"></img>
              <p>
                Ésta planta se modificó, y se le añadió un baño que completara el diseño de dicha habitación. El diseño de 
                la habitación resultante se enfoca en transmitir transparencia y reflexión mediante espejos, éste concepto 
                procede del principal referente, "The Mirror Cube". Éste referente es una habitación perteneciente al 
                hotel "Tree Hotel", éste hotel consta de varias habitaciones en árboles, la habitación elegida es un cubo de 
                espejos que se mimetiza con el entorno, dejando una ilusión de transparencia en el bosque.
              </p>
              
              <p>La distribución resultante del diseño es de apariencia diáfana, que se complementa con el concepto de la 
                transparencia mediante diversos espejos dispuestos por la habitación.
                La habitación consta de un vestidor con espejo de suelo a techo y puertas correderas recubiertas de espejos, 
                un baño con inodoro, pila de pié y bañera, de color blanco transmitiendo sensación de pureza y luminosidad. 
                La estancia principal se conforma de diversas zonas cohesionadas mediante el uso de los espejos y del color 
                azul, que nos recuerda al cielo azul.</p>
              <p>Nada más entrar se puede observar la habitación al completo, donde destaca una cama estilo "bed up" con dos mesillas 
                de noche i una televisión colgada, esta zona, se consideraría el espacio de descanso. Junto a ella se observa un banco de 
                ventana, una silla colgante y más a la derecha un sofá, todo ello forma la zona social de la estancia. Por último, junto al 
                acceso se encuentra un escritorio con una mini nevera integrada en dicha mesa, esto se consideraría la zona de trabajo y 
                mini bar.</p>
                <img className="image big center" src="../../Hotel_2017/Planta_Textura.JPG" alt="PlantaTextura"></img>
              <br></br>
              <p>
              Los materiales y texturas que destacan en la estancia son el micro-cemento pulido presente en todo el suelo, combinado con 
              dos franjas de césped que dan un toque natural y acogedor a la neutralidad del micro-cemento. A ello se le suman las transparencias 
              y reflexiones del metacrilato y vidrios azules que nos recuerdan al cielo y los espejos que nos unen al concepto de transparencia.
              </p>
              <div className="sub-content">
                  {<div className="sub1">
                      <h2>Planimetria del Proyecto</h2>
                      <div className="slides_container"> 
                        <Carousel images={{prefix:"Hotel_2017/Perimetria",names:["AlçatA.JPG","AlçatB.JPG"]}}></Carousel>
                      </div>
                  </div>}
                  {<div className="sub3">
                      <h2>Renders del Proyecto</h2>
                      <div className="slides_container"> 
                        <Carousel images={{prefix:"Hotel_2017/Renders",names:["2_Escriptori.JPG","3_Social2.JPG"]}}></Carousel>
                      </div>
                  </div>}
              </div>
          {/* Contenido a borrar en nueva pagina */}
        </div>
      </section>
</section>
</Layout>;

export default Hotel;
