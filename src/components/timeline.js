import React from "react"
import config from '../../config';
const addItem = (params) =>{
    return [
        <li key={Math.random()}>
            <div className="m-timeline__date">
                {params.time}-{params.tittle}
            </div>

            <p>{params.desc}</p>
        </li>

    ]
}
const Timeline = () => {
    return(
        <div className="m-content">
            <ul className="m-timeline">
            {
                config.time_line.map((item) => {
                    const {direction, tittle, time ,desc} = item;
                    return addItem({direction:direction,tittle:tittle, time:time ,desc:desc})
                })
            }
            </ul>
        </div>
    )
    

}

export default Timeline;