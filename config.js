module.exports = {
  siteTitle: 'Test Sara CV', // <title>
  manifestName: 'Viaje antes que destino',
  manifestShortName: 'Landing', // max 12 characters
  manifestStartUrl: '/',
  manifestBackgroundColor: '#663399',
  manifestThemeColor: '#663399',
  manifestDisplay: 'standalone',
  manifestIcon: 'src/assets/img/website-icon.png',
  pathPrefix: `/gatsby-starter-solidstate/`, // This path is subpath of your hosting https://domain/portfolio
  heading: ' AAAAAAAAAAAA ',
  subHeading: 'Vida antes que Muerte. Viaje antes que destino. Fuerza antes que debilidad. ',
  // social
  socialLinks: [
    {
      icon: 'fab fa-instagram',
      name: 'Instagram',
      color:"#EA4C89",
      url: 'https://www.instagram.com/sarainteriorss/',
    },
    {
      icon: 'fa-twitter',
      name: 'Twitter',
      color:"#00ACED",
      url: 'https://twitter.com/',
    },
  ],
  skills:[
    {name:"Photoshop",progress:"10",color:"353c4e"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
    {name:"Autocad",progress:"30",color:"4a4e59"},
  ],
  works:[
    {title:"Hotel 2017",subtitle:"Creación de una habitación de hotel para la segunda edición del concurso InterCIDEC",
     image:"../../Hotel_2017/Vista_General.JPG",pageRoute:"../Portfolio/Hotel"},
     {title:"Test2",subtitle:"Esto es una prueba de como crear una entrada dinamica",
     image:"../../bg3.jpg",pageRoute:"../Portfolio/"},
     {title:"Test3",subtitle:"Esto es una prueba de como crear una entrada dinamica",
     image:"../../bg4.jpg",pageRoute:"../Portfolio/"},
     {title:"Test4",subtitle:"Esto es una prueba de como crear una entrada dinamica",
     image:"../../bg4.jpg",pageRoute:"../Portfolio/"}, /* Imagen en grande cuando estamos en el Home */

  ],
  time_line:[
    {tittle:"Enseñanza1",time:"1998",desc:"ahkagdkhsvd kgadhkasdhydsjbhsgbcgsgdi"},
    {tittle:"Enseñanza2",time:"1998",desc:"ahkagdkhsvd kgadhkasdhydsjbhsgbcgsgdi"},
    {tittle:"Enseñanza3",time:"1998",desc:"ahkagdkhsvd kgadhkasdhydsjbhsgbcgsgdi"},
    {tittle:"Enseñanza4",time:"1998",desc:"ahkagdkhsvd kgadhkasdhydsjbhsgbcgsgdi"},
  ]
  
};
